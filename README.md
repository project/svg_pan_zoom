# Svg Pan Zoom

The Svg Pan Zoom module provides an integration of the Svg Pan Zoom library.
It provides you a field formatter for your Image fields that supports SVGs.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/svg_pan_zoom).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/svg_pan_zoom).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This requirements for this module are:

- The Svg Pan Zoom library (`https://github.com/ariutta/svg-pan-zoom`)
   The module supports the 3.6.1 release or higher.
- The Svg image Drupal module (`https://www.drupal.org/project/svg_image`)


## Installation

Add the Svg Pan Library to your libraries folder.
   You can add the library through composer:
   "repositories": {
     {
       "type": "package",
       "package": {
           "name": "library/svg-pan-zoom",
           "version": "3.6.1",
           "type": "drupal-library",
           "dist": {
             "url": "https://github.com/ariutta/svg-pan-zoom/archive/3.6.1.zip",
             "type": "zip"
           }
       }
     }
   },
  "require": {
    "library/svg-pan-zoom": "3.6.1"
  }

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Select the Svg Pan Zoom widget in your Entity Display for your image field.
2. Configure the Svg Pan Zoom widget as you want


## Maintainers

- Philippe Joulot - [phjou](https://www.drupal.org/u/phjou)
