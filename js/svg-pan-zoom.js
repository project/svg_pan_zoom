/**
 * @file
 * Contains the definition of the behaviour svg_pan_zoom.
 */

(function (drupalSettings, Drupal) {

  'use strict';

  Drupal.behaviors.svgPanZoom = {
    attach: function (context) {
      var svgs = context.querySelectorAll('.js-svg-pan-zoom');
      if (svgs.length > 0) {
        for (var i = 0; i < svgs.length; ++i) {
          // We are checking if the container is visible.
          // There are multiple problems with the library when an svg is hidden.
          // If you are in that case, we defined the function
          // Drupal.svgPanZoom.createElement in order to easily call the
          // library at the right moment yourself.
          if (svgs[i].dataset['display'] !== 'inline' || svgs[i].offsetHeight > 0) {
            Drupal.svgPanZoom.createElement(svgs[i]);
          }
        }
      }
    }
  };

  Drupal.svgPanZoom = {
    createElement: function (element) {
      // Build the configuration.
      var options = {};
      const dataNames = Object.keys(drupalSettings.svgPanZoom.availableOptions);
      for (var j = 0; j < dataNames.length; ++j) {
        var dataName = dataNames[j];
        var optionName = drupalSettings.svgPanZoom.availableOptions[dataName];
        var value = element.dataset[dataName];
        if (value === 'false' || value === 'true') {
          options[optionName] = value === 'true';
        }
        else if (!isNaN(value)) {
          options[optionName] = parseFloat(value);
        }
        else {
          options[optionName] = value;
        }
      }

      // Apply the library on the svg.
      if (element.dataset['display'] === 'inline') {
        var target_element = element.querySelector('svg');
        svgPanZoom(target_element, options);
      }
      else {
        element.addEventListener('load', function () {
          svgPanZoom(this, options);
        });
      }
    }
  };

})(drupalSettings, Drupal);
