<?php

namespace Drupal\svg_pan_zoom\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'scg pan zoom' formatter.
 *
 * @FieldFormatter(
 *   id = "svg_pan_zoom",
 *   label = @Translation("Svg Pan Zoom"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class SvgPanZoomFormatter extends ImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $other_settings = [
      'width' => '',
      'height' => '',
      'display' => 'embed',
    ];
    return $other_settings + self::getDefaultSvgPanZoomSettings() + parent::defaultSettings();
  }

  /**
   * Function to get the default svg pan zoom settings.
   *
   * @return array
   *   The array with the default values of the svg pan zoom settings.
   */
  public static function getDefaultSvgPanZoomSettings() {
    return [
      'pan_enabled' => TRUE,
      'control_icons_enabled' => FALSE,
      'zoom_enabled' => TRUE,
      'dbl_click_zoom_enabled' => TRUE,
      'mouse_wheel_zoom_enabled' => TRUE,
      'prevent_mouse_events_default' => TRUE,
      'zoom_scale_sensitivity' => 0.2,
      'min_zoom' => 0.5,
      'max_zoom' => 10,
      'fit' => TRUE,
      'contain' => FALSE,
      'center' => TRUE,
      'refresh_rate' => 'auto',
    ];
  }

  /**
   * Function to get the svg pan zoom option names.
   *
   * @return array
   *   The array with the names of the svg pan zoom options.
   */
  public static function getSvgPanZoomOptions() {
    return [
      'pan_enabled' => 'panEnabled',
      'control_icons_enabled' => 'controlIconsEnabled',
      'zoom_enabled' => 'zoomEnabled',
      'dbl_click_zoom_enabled' => 'dblClickZoomEnabled',
      'mouse_wheel_zoom_enabled' => 'mouseWheelZoomEnabled',
      'prevent_mouse_events_default' => 'preventMouseEventsDefault',
      'zoom_scale_sensitivity' => 'zoomScaleSensitivity',
      'min_zoom' => 'minZoom',
      'max_zoom' => 'maxZoom',
      'fit' => 'fit',
      'contain' => 'contain',
      'center' => 'center',
      'refresh_rate' => 'refreshRate',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
    ];

    $element['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
    ];

    $element['display'] = [
      '#title' => $this->t('Display'),
      '#type' => 'select',
      '#options' => [
        'embed' => $this->t('Embed'),
        'inline' => $this->t('Inline'),
      ],
      '#default_value' => $this->getSetting('display'),
      '#description' => $this->t('Choose how you prefer to render your svg.'),
    ];

    $element['refresh_rate'] = [
      '#title' => $this->t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
    ];

    $element['pan_enabled'] = [
      '#title' => $this->t('panEnabled'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('pan_enabled'),
    ];

    $element['control_icons_enabled'] = [
      '#title' => $this->t('controlIconsEnabled'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('control_icons_enabled'),
    ];

    $element['zoom_enabled'] = [
      '#title' => $this->t('zoomEnabled'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('zoom_enabled'),
    ];

    $element['dbl_click_zoom_enabled'] = [
      '#title' => $this->t('dblClickZoomEnabled'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('dbl_click_zoom_enabled'),
    ];

    $element['mouse_wheel_zoom_enabled'] = [
      '#title' => $this->t('mouseWheelZoomEnabled'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('mouse_wheel_zoom_enabled'),
    ];

    $element['prevent_mouse_events_default'] = [
      '#title' => $this->t('preventMouseEventsDefault'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('prevent_mouse_events_default'),
    ];

    $element['zoom_scale_sensitivity'] = [
      '#title' => $this->t('zoomScaleSensitivity'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('zoom_scale_sensitivity'),
      '#min' => 0,
      '#step' => 'any',
      '#required' => TRUE,
    ];

    $element['min_zoom'] = [
      '#title' => $this->t('minZoom'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('min_zoom'),
      '#min' => 0,
      '#step' => 'any',
      '#required' => TRUE,
    ];

    $element['max_zoom'] = [
      '#title' => $this->t('maxZoom'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('max_zoom'),
      '#min' => 0,
      '#step' => 'any',
      '#required' => TRUE,
    ];

    $element['fit'] = [
      '#title' => $this->t('fit'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('fit'),
    ];

    $element['contain'] = [
      '#title' => $this->t('contain'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('contain'),
    ];

    $element['center'] = [
      '#title' => $this->t('center'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('center'),
    ];

    $element['refresh_rate'] = [
      '#title' => $this->t('refreshRate'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('refresh_rate'),
    ];

    $element['help'] = [
      '#markup' => $this->t('For more information about each option, please read the documentation: <a href="@doc">See more</a>', ['@doc' => 'https://github.com/ariutta/svg-pan-zoom']),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $base_cache_tags = [];

    foreach ($files as $delta => $file) {
      $cache_contexts = [];
      $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $item_attributes['class'] = ['js-svg-pan-zoom'];

      $default_options = $this->getDefaultSvgPanZoomSettings();
      $options = array_keys($default_options);
      foreach ($options as $option) {
        $setting = $this->getSetting($option);
        if (is_bool($default_options[$option])) {
          $item_attributes['data-' . $option] = ($setting) ? 'true' : 'false';
        }
        else {
          $item_attributes['data-' . $option] = $this->getSetting($option);
        }
      }
      $item_attributes['data-display'] = $this->getSetting('display');

      if (!empty($this->getSetting('width'))) {
        $item_attributes['width'] = $this->getSetting('width');
      }
      if (!empty($this->getSetting('height'))) {
        $item_attributes['height'] = $this->getSetting('height');
      }

      $elements[$delta] = [
        '#theme' => 'svg_pan_zoom_formatter',
        '#item' => $item,
        '#display' => $this->getSetting('display'),
        '#item_attributes' => $item_attributes,
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
          'contexts' => $cache_contexts,
        ],
        '#attached' => [
          'library' => ['svg_pan_zoom/svg-pan-zoom'],
          'drupalSettings' => [
            'svgPanZoom' => [
              'availableOptions' => $this->getSvgPanZoomOptions(),
            ],
          ],
        ],
      ];
    }

    return $elements;
  }

}
